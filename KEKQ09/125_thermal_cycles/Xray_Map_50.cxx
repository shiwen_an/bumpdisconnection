// Author: Shiwen An
// Date: 2021/06/29
// Purpose: The Macro for readout X-ray map, with Maximum 50 setup

using namespace std;

void Xray_Map_50(){
    const Int_t NRGBs = 5;    
  const Int_t NCont = 255;  
 
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t Red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 }; 
  Double_t Green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 }; 
  Double_t Blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 }; 
  TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
  gStyle->SetNumberContours(NCont);

  std::cout<<""<<endl;
  TFile* chip1 = new TFile("chip1.root");

  TFile* chip2 = new TFile("chip2.root");
  TFile* chip3 = new TFile("chip3.root");
  TFile* chip4 = new TFile("chip4.root");
  
  TH2F*Map1 = (TH2F*) chip1->Get("chip1/Occupancy/Map");
  
  TH2F*Map2 = (TH2F*) chip2->Get("chip2/Occupancy/Map");
  TH2F*Map3 = (TH2F*) chip3->Get("chip3/Occupancy/Map");
  TH2F*Map4 = (TH2F*) chip4->Get("chip4/Occupancy/Map");
  /*
  Map1->GetXaxis()->SetRangeUser(133,399);
  Map1->GetXaxis()->SetRangeUser(133,399);
  Map2->GetXaxis()->SetRangeUser(133,399);
  Map3->GetXaxis()->SetRangeUser(133,399);
  Map4->GetXaxis()->SetRangeUser(133,399);
  */

  TCanvas* cc = new TCanvas();
  cc->Divide(2,2);
  cc->SetGridx();
  cc->SetGridy();
  cc->cd(1); 
  // Could be setup individually 
  Map1->SetMaximum(50);
  Map1->SetTitle("Chip1 Xray Max 50");
  Map1->Draw("coltz");
  cc->cd(3);
  Map2->SetMaximum(50);
  Map2->SetTitle("Chip2 Xray Max 50");
  Map2->Draw("coltz");
  cc->cd(4);
  Map3->SetMaximum(50);
  Map3->SetTitle("Chip3 Xray Max 50");
  Map3->Draw("coltz");
  cc->cd(2);
  Map4->SetMaximum(50);
  Map4->SetTitle("Chip4 Xray Max 50");
  Map4->Draw("coltz");
  // 
  
}
