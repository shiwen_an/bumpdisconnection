// Author: Shiwen An
// Date: 2021/09/29
// Purpose: Comparison of the Xray Result and Thermal Cycle result

using namespace std;
void compare_xray(){
	const Int_t NRGBs = 5;
	const Int_t NCont = 255;

	Double_t stops[NRGBs] = { 0.00, 0.34, 0.66, 0.87, 1.00 };
 	Double_t Red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.91 };
 	Double_t Green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.1 };
 	Double_t Blue[NRGBs]  = { 0.00, 1.00, 0.12, 0.00, 0.00 };
	TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
	gStyle->SetNumberContours(NCont);
	
	TCanvas* c1 = new TCanvas();
	//gStyle->SetPalette(kLightTemperature);
	
	TFile* f1 = new TFile("chip2.root");
  	TFile* AllMap = new TFile("rootfile.root");
	
	//f1->ls();
	TH2F* map1 = (TH2F*)f1->Get("chip2/Occupancy/Map");
	TH2F* map2 = (TH2F*)AllMap->Get("chip2/OccupancyMap/Map");
	//map1->SetMaximum(50);
        //map2->SetMaximum(50);	
         
        TH2F* comp = new TH2F("map1","Xray& BumpDisconnection Similarity",400,0,400,192, 0, 192);
		
        TH2F* comp1 = new TH2F("map2","Xray",400,0,400,192,0,192);
	
        TH2F* comp2 = new TH2F("map3","BumpDisconnection",400,0,400,192,0,192);

	/*
        for(int i = 1; i<=400;i++){
	  for(int j=1; j<=192;j++){
            //std::cout<<"i: "<<i<<","<<"j: "<<j<<","<< map1->GetBinContent(i,j)<<"\n"; 
            //std::cout<<"i: "<<i<<","<<"j: "<<j<<","<< map2->GetBinContent(i,j)<<"\n"; 
	    if(map1->GetBinContent(i,j)== map2->GetBinContent(i,j) && map1->GetBinContent(i,j)==0) {comp->Fill(i,j);
              std::cout<<"i: "<<i<<","<<"j: "<<j<<","<<map1->GetBinContent(i,j)<<"\n"; 
            }
	    if(map1->GetBinContent(i,j)==0) comp1->Fill(i,j);
	    if(map2->GetBinContent(i,j)==0) comp2->Fill(i,j);
             
	  }
	}
*/
       
       // No Syn FE version
	

        for(int i = 133; i<=400;i++){
	  for(int j=1; j<=192;j++){
            //std::cout<<"i: "<<i<<","<<"j: "<<j<<","<< map1->GetBinContent(i,j)<<"\n"; 
            //std::cout<<"i: "<<i<<","<<"j: "<<j<<","<< map2->GetBinContent(i,j)<<"\n"; 
	    if(map1->GetBinContent(i,j)== map2->GetBinContent(i,j) && map1->GetBinContent(i,j)==0) {comp->Fill(i,j);
              std::cout<<"i: "<<i<<","<<"j: "<<j<<","<<map1->GetBinContent(i,j)<<"\n"; 
            }
	    if(map1->GetBinContent(i,j)==0) comp1->Fill(i,j);
	    if(map2->GetBinContent(i,j)==0) comp2->Fill(i,j);
             
	  }
	}
	comp->Draw("colz");
	TCanvas * c2 = new TCanvas();
	comp1->Draw("colz");
	TCanvas * c3 = new TCanvas();
	comp2->Draw("colz");
	
	//gStyle->SetOptStat(1);
	//c1->Update();
	c1->Print("xray_bump.png");
	c2->Print("xray.png");
	c3->Print("bump.png");
        
	//f1->Close();
	//f2->Close();
}
