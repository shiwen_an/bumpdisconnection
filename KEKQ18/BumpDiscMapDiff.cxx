// Author: Shiwen An
// Date: 2021/09/27
// Purpose: The Macro for readout Occupancy Map for Bump Disconnection Scan

using namespace std;

void BumpDiscMapDiff(){
  const Int_t NRGBs = 5;    
  const Int_t NCont = 255;  
 
  
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.66, 0.87, 1.00 };
  Double_t Red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.91 }; 
  Double_t Green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.91 }; 
  Double_t Blue[NRGBs]  = { 0.00, 1.00, 0.12, 0.00, 0.00 }; 
  TColor::CreateGradientColorTable(NRGBs, stops, Red, Green, Blue, NCont);
  gStyle->SetNumberContours(NCont);
  

  std::cout<<""<<endl;
  TFile* AllMap = new TFile("1000eBumpDisc.root");

  
  TH2F*Map1 = (TH2F*) AllMap->Get("chip1/OccupancyMap/Map_Diff"); 
  TH2F*Map2 = (TH2F*) AllMap->Get("chip2/OccupancyMap/Map_Diff");
  TH2F*Map3 = (TH2F*) AllMap->Get("chip3/OccupancyMap/Map_Diff");
  TH2F*Map4 = (TH2F*) AllMap->Get("chip4/OccupancyMap/Map_Diff");
  /*
  Map1->GetXaxis()->SetRangeUser(133,399);
  Map1->GetXaxis()->SetRangeUser(133,399);
  Map2->GetXaxis()->SetRangeUser(133,399);
  Map3->GetXaxis()->SetRangeUser(133,399);
  Map4->GetXaxis()->SetRangeUser(133,399);
  */

  TCanvas* cc = new TCanvas("c","c",3680,480);
  cc->Divide(4,1);
  cc->SetGridx();
  cc->SetGridy();
  cc->cd(1); 
  // Could be setup individually 
  Map1->SetTitle("Chip1 BumpDisc Map Diff FE");
  Map1->Draw("coltz");
  cc->cd(2);
  Map2->SetTitle("Chip2 BumpDisc Map Diff FE");
  Map2->Draw("coltz");
  cc->cd(3);
  Map3->SetTitle("Chip3 BumpDisc Map Diff FE");
  Map3->Draw("coltz");
  cc->cd(4);
  Map4->SetTitle("Chip4 BumpDisc Map Diff FE");
  Map4->Draw("coltz");
  // 
  
}
